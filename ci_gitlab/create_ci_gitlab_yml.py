#! /usr/bin/env python3


#
# Where to write the gitlab_ci_file to
#
gitlab_ci_file="ci_gitlab.yml"

#
# SWEET's root directory
#
# Relative to this path, the tests are searched for
#
sweet_root_directory="../../sweet"


####################################################
# Setup finishes here
####################################################

import sys
import os
import glob
import re
from itertools import product

# Import all test environment descriptions
from test_environments import *


verbosity = 10
if len(sys.argv) > 1:
    verbosity = int(sys.argv[1])

if verbosity >= 10:
    print("Working directory: "+os.path.abspath(os.curdir))
    print("Setting up tests in Gitlab CI file '"+gitlab_ci_file+"'")


#
# Load all tests
#
tests = glob.glob(f"{sweet_root_directory}/tests/??_*/test.sh")
tests += glob.glob(f"{sweet_root_directory}/tests/??_*/test.py")

# Sort tests
tests.sort()

def get_test_id(test):
    r = re.match(r".*/([0-9_a-z]*)/test\.[sp][hy]", test)
    if r == None:
        raise Exception("Invalid test file found")

    return r.group(1)

def get_test_id_with_script(test):
    r = re.match(r".*/([0-9_a-z]*/test\.[sp][hy])", test)
    if r == None:
        raise Exception("Invalid test file found")

    return r.group(1)


job_counter = 0

if verbosity >= 10:
    for test in tests:
        test_id = get_test_id(test)
        print(f" + Found test script '{test}' with id '{test_id}'")

if verbosity >= 10:
    print("Writing content to file '"+gitlab_ci_file+"'")


content = ""

content += """\
#
# Gitlab CI file for SWEET
#
# Warning! This file is automatically generated. DO NOT MODIFY!
#

include: 
  - ci_gitlab/templates.yml

stages:          # List of stages for jobs, and their order of execution
  - stage-docker-build
"""

for te in test_environment_:
    if te['gen_ci_tests']:
        content += f"""\
  - stage_tests_{te['id']}
"""

content += f"""\

"""


#
# Setup Docker Images with local software for each environment
#
# These Docker Images are only build if
#
# * the commit message includes
#   [docker-build]
#   including the [ ] brackets
#   or
#
# * there's a tag
#
# This is part of the file template.yml
#
for te in test_environment_:

    script_header = f"""\
    - export CC={te['env_CC']}
    - export CXX={te['env_CXX']}
    - export F90={te['env_F90']}
    - export FC={te['env_FC']}
    - export LD={te['env_LD']}
"""

    content += f"""\

setup-docker-images-{te['id']}:       # This job runs in the build stage, which runs first.
{te['job_tags_docker_build']}\
  stage: stage-docker-build
  variables:
    IMAGE_NAME: sweet-{te['id']}
  extends:
    .docker-build
"""

    job_counter += 1


#
# Generate individual test cases
#

#if False:
#for te in test_environment_:
    if te['gen_ci_tests']:
        for test in tests:

            job_id = get_test_id(test)

            content += f"""\

# Template for this job
.test-{te['id']}-{job_id}:
  stage: stage_tests_{te['id']}
  image: $CI_REGISTRY_IMAGE/sources/sweet-{te['id']}
{te['job_tags']}\
  script:
{script_header}\
    - cd "{sweet_docker_dir}"
    - git clone --depth 1 https://github.com/schreiberx/sweet.git sweet_src
    - cd sweet_src
    - mv ../local_backup ./local_software/local/
    - source ./activate.sh
    - ./tests/{get_test_id_with_script(test)}
    - echo "TEST {job_id} SUCCESSFULLY FINISHED"


# With Docker image DAG dependency
test-{te['id']}-{job_id}:
  # Job dependency
  needs: ["setup-docker-images-{te['id']}"]
  extends:
    - .test-{te['id']}-{job_id}
    - .rules-tests-norun-and-docker-rules


# WITHOUT Docker Image DAG dependency 
test-{te['id']}-{job_id}_:
  # Job dependency
  needs: []
  extends:
    - .test-{te['id']}-{job_id}
    - .rules-tests-norun-and-docker-rules-inv
"""
            job_counter += 1


print(content)


with open(gitlab_ci_file, 'w') as f:
    f.write(content)


if verbosity >= 10:
    print(f"Job counter: {job_counter}")

