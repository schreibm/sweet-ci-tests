FROM ubuntu:focal
SHELL ["/bin/bash", "-c"]
ENV TZ=Europe/Paris


RUN \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && apt-get update \
    && apt-get install -y -qq git make automake cmake python3 wget libssl-dev pkg-config libfreetype-dev libopengl-dev libglu1-mesa-dev libxext-dev  g++-10 gcc-10 gfortran-10 \
    && apt-get autoclean -y \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*


ENV CC=gcc-10
ENV CXX=g++-10
ENV F90=gfortran-10
ENV FC=gfortran-10
ENV LD=ld

RUN echo "Creating SWEET directory to install software to"  \
    && mkdir -p "/sweet_docker"   \
    && echo "FIN"

RUN echo "Cloning SWEET git repository" \
    && cd "/sweet_docker" \
    && git clone --depth 1 https://github.com/schreiberx/sweet.git "/sweet_docker/sweet_src" \
    && echo "FIN"


RUN echo "Installing local software with 'install_miniconda.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_miniconda.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_fftw3.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_fftw3.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_shtns.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_shtns.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_shtns_python.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_shtns_python.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_scons.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_scons.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_mpich.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_mpich.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_eigen3.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_eigen3.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_lapack.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_lapack.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_numactl.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_numactl.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_libpfasst_debug.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_libpfasst_debug.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_xbraid.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_xbraid.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    
RUN echo "Installing local software with 'install_sdl2.sh'" \
    && cd "/sweet_docker/sweet_src" \
    && . ./activate.sh gitlab_ci \
    && cd local_software \
    && time   ./install_sdl2.sh \
    && rm -rf ./local_src \
    && echo "DONE"

    

RUN echo "Creating backup of installed SWEET local software..." \
    && cd "/sweet_docker" \
    && mv "sweet_src/local_software/local" "./local_backup" \
    && echo "DONE"

# Cleaning up things a little bit
RUN echo "Cleaning up..." \
    && cd "/sweet_docker" \
    && rm -rf sweet_src \
    && echo "DONE"

# We're ready to use SWEET
