#! /bin/bash


for i in dockerfiles/*; do
	echo "Building with dockerfile in '$i'"
	docker build "$i/"
done
