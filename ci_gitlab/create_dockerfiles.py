#! /usr/bin/env python3

import sys
import os

from test_environments import *



#
# Docker image builders
#
for te in test_environment_:
    script_header = f"""\
    # Avoid interaction by setting 
    - export DEBIAN_FRONTEND=noninteractive
    # Install packages
"""

    content = f"""\
FROM {te['docker_image_version']}
SHELL ["/bin/bash", "-c"]
ENV TZ=Europe/Paris

{te['runs_install_packages']}

ENV CC={te['env_CC']}
ENV CXX={te['env_CXX']}
ENV F90={te['env_F90']}
ENV FC={te['env_FC']}
ENV LD={te['env_LD']}

RUN echo "Creating SWEET directory to install software to"  \\
    && mkdir -p "{sweet_docker_dir}"   \\
    && echo "FIN"

RUN echo "Cloning SWEET git repository" \\
    && cd "{sweet_docker_dir}" \\
    && git clone --depth 1 https://github.com/schreiberx/sweet.git "{sweet_docker_dir}/sweet_src" \\
    && echo "FIN"

{te['runs_install_local_software']}

RUN echo "Creating backup of installed SWEET local software..." \\
    && cd "{sweet_docker_dir}" \\
    && mv "sweet_src/local_software/local" "./local_backup" \\
    && echo "DONE"

# Cleaning up things a little bit
RUN echo "Cleaning up..." \\
    && cd "{sweet_docker_dir}" \\
    && rm -rf sweet_src \\
    && echo "DONE"

# We're ready to use SWEET
"""

    dir_path = f"dockerfiles/sweet-{te['id']}/"
    print(f"Setting up dockerfile in '{dir_path}'")

    try:
        os.makedirs(dir_path)
    except:
        pass

    with open(f"{dir_path}/Dockerfile", "w") as f:
        f.write(content)

    print(f"Test by running: $ docker build {dir_path}")
