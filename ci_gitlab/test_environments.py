
from config import *


# Which directory to use as a common directory setup things
sweet_docker_dir = "/sweet_docker"


"""
List of dictionaries describing the test environments
"""
test_environment_ = []


ubuntu_version_ = [18, 20, 22]

# Setup configurations for all different ubuntu versions and compilers
for ubuntu_version in ubuntu_version_:

    # Particular image version - named differently on different CI systems
    docker_image_version = None
    if gitlab_ci_kind == "gricad":
        # bionic: 18.04
        # focal: 20.04
        # jammy: 22.04
        if ubuntu_version == 18:
            docker_image_version = "ubuntu:bionic"
        elif ubuntu_version == 20:
            docker_image_version = "ubuntu:focal"
        elif ubuntu_version == 22:
            docker_image_version = "ubuntu:jammy"
        else:
            raise Exception("Unsupported version")

    elif gitlab_ci_kind == "inria":
        if ubuntu_version == 18:
            docker_image_version = "library/ubuntu:18.04"
        elif ubuntu_version == 20:
            docker_image_version = "library/ubuntu:20.04"
        elif ubuntu_version == 22:
            docker_image_version = "library/ubuntu:22.04"
        else:
            raise Exception("Unsupported version")

    # Graphics packages vary in Ubuntu versions
    apt_get_graphics_pkgs = None
    if ubuntu_version == 18:
        apt_get_graphics_pkgs = "pkg-config libfreetype6-dev libglu1-mesa-dev"
    elif ubuntu_version == 20:
        apt_get_graphics_pkgs = "pkg-config libfreetype-dev libopengl-dev libglu1-mesa-dev libxext-dev"
    elif ubuntu_version == 22:
        apt_get_graphics_pkgs = "pkg-config libfreetype-dev libopengl-dev libglu1-mesa-dev libxext-dev"
    else:
        raise Exception("Unsupported version")


    # Standard apt-get packages
    apt_get_default_packages = "git make automake cmake python3 wget libssl-dev"


    # Special job tags for INRIA CI
    job_tags = ""
    job_tags_docker_build = ""
    if gitlab_ci_kind == "inria":
        job_tags = """\
  tags:
    - ci.inria.fr
    - large
"""

        # Use the same tags for creating the docker images
        job_tags_docker_build = job_tags[:]


    def get_runs_install_packages(apt_get_packages):
        return f"""
RUN \\
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \\
    && apt-get update \\
    && apt-get install -y -qq {apt_get_packages} \\
    && apt-get autoclean -y \\
    && apt-get autoremove -y \\
    && rm -rf /var/lib/apt/lists/*
"""



    def get_run_local_software_install(local_software_install_script, env_vars="", taskset=""):
        return  f"""
RUN echo "Installing local software with '{local_software_install_script}'" \\
    && cd "{sweet_docker_dir}/sweet_src" \\
    && . ./activate.sh gitlab_ci \\
    && cd local_software \\
    && time {env_vars} {taskset} ./{local_software_install_script} \\
    && rm -rf ./local_src \\
    && echo "DONE"

    """
    def get_runs_install_local_software():
        default_install_scripts_ = [
               'install_miniconda.sh',
               'install_fftw3.sh',
               'install_shtns.sh',
               'install_shtns_python.sh',
               'install_scons.sh',
               'install_mpich.sh', 
               'install_eigen3.sh',
               'install_lapack.sh',
               'install_numactl.sh',
               'install_libpfasst_debug.sh',
               'install_xbraid.sh',
               'install_sdl2.sh',
           ]

        retval = ""

        for _ in default_install_scripts_:
            retval += get_run_local_software_install(_)

        return retval



    #for compiler in ['gcc', 'llvm']:
    for compiler in ['llvm']:

        if compiler == 'gcc':

            # Available Ubuntu compiler versions
            if ubuntu_version == 18:
                #gcc_version_ = [6,7,8]
                # GCC Version 6 leads to shtns compilation problems (internal compiler error)
                gcc_version_ = [7,8]
            elif ubuntu_version == 20:
                gcc_version_ = [8,9,10]
            elif ubuntu_version == 22:
                gcc_version_ = [9,10,11,12]


            for gcc_version in gcc_version_:

                # Add GCC compiler packages
                apt_get_packages_gcc = f" g++-{gcc_version} gcc-{gcc_version} gfortran-{gcc_version}"

                apt_get_packages = f"{apt_get_default_packages} {apt_get_graphics_pkgs} {apt_get_packages_gcc}"

                runs_install_packages = get_runs_install_packages(apt_get_packages)

                runs_install_local_software = get_runs_install_local_software()

                #
                # Generate individual CI tests
                # only for these particular environments
                #
                gen_ci_tests = False
                if ubuntu_version == 22 and gcc_version == 12:
                    gen_ci_tests = True
                elif ubuntu_version == 20 and gcc_version == 10:
                    gen_ci_tests = True
                elif ubuntu_version == 20 and gcc_version == 8:
                    gen_ci_tests = True
                elif ubuntu_version == 18 and gcc_version == 6:
                    gen_ci_tests = True


                test_environment_ += [{
                            'id': f"ubu{ubuntu_version}-gcc{gcc_version}",
                            'docker_image_version': docker_image_version,
                            'job_tags': job_tags,
                            'job_tags_docker_build': job_tags_docker_build,

                            'env_CC': f"gcc-{gcc_version}",
                            'env_CXX': f"g++-{gcc_version}",
                            'env_F90': f"gfortran-{gcc_version}",
                            'env_FC': f"gfortran-{gcc_version}",
                            'env_LD': f"ld",

                            'runs_install_packages': runs_install_packages,

                            'runs_install_local_software': runs_install_local_software,

                            'gen_ci_tests': gen_ci_tests,
                        }]


        elif compiler == 'llvm':

            # Override job tags to use our own 'very-large' Gitlab runner since we will compile LLVM
            job_tags = ""
            if gitlab_ci_kind == "inria":
                job_tags_docker_build = """\
  tags:
    - ci.inria.fr
    - very-large
"""


            # Available Ubuntu compiler versions
            if ubuntu_version == 18:
                llvm_compile_gcc_version = 8
                llvm_version_ = [15]
            elif ubuntu_version == 20:
                llvm_compile_gcc_version = 10
                llvm_version_ = [15]
            elif ubuntu_version == 22:
                llvm_compile_gcc_version = 12
                llvm_version_ = [15]

            # Compile LLVM with latest installed GCC version
            apt_get_packages_gcc = f" g++-{llvm_compile_gcc_version} gcc-{llvm_compile_gcc_version} gfortran-{llvm_compile_gcc_version}"

            apt_get_packages = f"{apt_get_default_packages} {apt_get_graphics_pkgs} {apt_get_packages_gcc}"

            runs_install_packages = get_runs_install_packages(apt_get_packages)

            for llvm_version in llvm_version_:

                taskset = ''
                # Limit number of tasks since we would run out of memory for compiling LLVM
                if gitlab_ci_kind == 'gricad':
                    taskset = 'taskset -c 0,1,2,3'
                if gitlab_ci_kind == 'inria':
                    taskset = 'taskset -c 0,1,2,3,4,5,6,7'

                if llvm_version == 15:
                    #
                    # We need to install cmake before llvm since llvm requires a newer cmake version
                    #
                    # Here, we also need to override the CC and CXX variables
                    #
                    # Version 15 installs 'flang-new' which we symlink in the end to flang-15
                    #
                    # We need to add -O2 to make C and Fortran compiled files compatible to each other
                    #

                    runs_install_local_software = ""

                    comp_env = f"CC=gcc-{llvm_compile_gcc_version} CXX=g++-{llvm_compile_gcc_version}"

                    # CMAKE is required in a newer version for  LLVM
                    runs_install_local_software += get_run_local_software_install("install_cmake.sh", comp_env)

                    # Install MiniConda before LLVM since some test scripts rely on a newer Python version
                    runs_install_local_software += get_run_local_software_install("install_miniconda.sh")

                    # Now, it's time to install LLVM
                    runs_install_local_software += get_run_local_software_install("install_llvm_15.0.6.sh", comp_env, taskset)
                    
                    runs_install_local_software += get_runs_install_local_software()

                #
                # Generate individual CI tests
                # only for these particular environments
                #
                gen_ci_tests = False
                if ubuntu_version == 22 and llvm_version == 15:
                    gen_ci_tests = True

                test_environment_ += [{
                            'id': f"ubu{ubuntu_version}-llvm{llvm_version}",
                            'docker_image_version': docker_image_version,
                            'job_tags': job_tags,
                            'job_tags_docker_build': job_tags_docker_build,

                            # Set C compiler to clang-*
                            'env_CC': f"clang-{llvm_version}",
                            # Set C compiler to clang-* (symlink from clang++)
                            'env_CXX': f"clang-{llvm_version}",
                            # Use GNU Fortran compiler, since MPICH doesn't support flang, yet
                            'env_F90': f"gfortran-{llvm_compile_gcc_version}",
                            'env_FC': f"gfortran-{llvm_compile_gcc_version}",

                            # Use default GNU linker 'ld'.
                            # LLVM's linker would be called 'ld.lld'
                            'env_LD': f"ld",

                            'runs_install_packages': runs_install_packages,

                            'runs_install_local_software': runs_install_local_software,

                            'gen_ci_tests': gen_ci_tests,
                        }]
